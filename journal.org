#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: Introduction au calcul parallèle
#+DATE: <2018-02-10 sam.>
#+AUTHOR: Grégory Mounié
#+EMAIL: Gregory.Mounie@imag.fr
#+LANGUAGE: fr
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 25.2.2 (Org mode 9.1.6)

* Contexte
  Un séminaire de formation à destination des enseignants du
  secondaire qui se forment en Informatique.

  Probablement environ 1h

  Important: ce sont des profs qui vont enseigner le
  parallélisme. C'est demandé dans le sujet. Comment faire ?

  Prendre un exemple bateau final: mandelbrot en python 3 ? Une image
  avec des bulles (en partant du TP systeme distribué, intro à MPI du
  make parallel de Fred Wagner ?)

  Le mandelbrot en Numpy, avec un découpage récursif, en cœur^2 zone
  par exemple ?
  doc denumpy en python3
  https://docs.scipy.org/doc/numpy-dev/user/quickstart.html
  

* Webographie rapide, Base de départ
** Les super cours d'Arnaud Legrand
   De la page http://mescal.imag.fr/membres/arnaud.legrand/teaching.php
   - l'introduction du cours de parallélisme pour master PC de 2009 (google): http://mescal.imag.fr/membres/arnaud.legrand/teaching/2009/01_parallel_architectures.pdf
     ou la dernière: http://mescal.imag.fr/membres/arnaud.legrand/teaching/2015/M2R_PC_parallel_architectures.pdf
   - un cours ISN, avec un exemple de système triangulaire en python: http://mescal.imag.fr/membres/arnaud.legrand/blog/2015/03/25/intervention_isn-gz.pdf
   - Ninja gap ? C'est quoi ?

* Des codes d'exemples
  - Quel langage: C++ (assez lisible ?), Julia (proche de python, mais
    jamais essayé ?), C et OpenMP (lisible ?)

    Il faudrait voir en Julia si cela fonctionne. Peut-on aussi faire
    facilement de l'échange de message en Julia (MPI ?)
  
    Perl 6 ? Ce serait rigolo, mais est-ce suffisament facile à lire ?

    Go: pas trop dur à lire, des performances, et une syntaxe proche
    des task ruby et python

    Parallelisme en Python ? Où en est le langage ? Il parait qu'il y
    a des async maintenant: en python 3 il semble y avoir pas mal de
    truc à lire la doc: thread, multiprocess, concurrent task
    le threading cython à un lock aussi.


    Python conseille multi-processing: on a la notion de Pipe, de
    Queue (quelle différence), la mémoire partagée (Array et Value),
    les lock et unlock. On doit pouvoir faire du celeri ou Elixir, ou
    MPI.

    
    

    Python + une lib parallèle pour l'échange de message ?

** A tester
   - faire les codes en python: quels performances ? quel lisibilité ?
   - regarder en Julia: vectorisation, synchro échange de message
** Julia: FAILED sur les threads
   - code facile à lire et écrire, mesure de temps ok
   - pas réussi à avoir un code simd malgré la directive en bulldozer,
     le code généré est sequentiel (=@code_llvm= permet de voir le
     code, rien de
   - pas si clair que cela pour illustrer le travail que cela demande
     pour la parallelization
   - FAILED: pas de programmation des threads en Julia !
     Se tourner vers Go pour les threads ? Perl 6 ? Rust ?
   - Par contre la distribution est ultra-simple (mais en C++ + boost MPI ?)

** Haskell ?
   Syntaxe un peu difficle pour un public pas habitué à ces notations ?

** OCaml
   Voir quelle performance l'on obtient

** En fortran ?
   Pour la vectorisation, voir la différence de performance avec C ?

** En numpy
   Pour avoir une idée de l'écart (peut-être faible ?)

** En résumé
*** Pour un produit scalaire vectoriel
   - Prendre le code Julia, montrer son temps d'exécution
   - Prendre le code C, montrer son temps d'exécution et le
     désassembler. Montrer l'écart avec le fast-math (les flottants ne
     sont pas associatifs)
   - Montrer aussi que les compilateurs sont idiots (perf du code
     fonctionnel) et qu'il faut souvent faire le travail à la main.

** Codes:
   - calcul d'un produit scalaire: vectorisation: Scala
   - i++ à plusieurs: notion de synchronisation: Version C++-14 avec 2 thread
   - i++ à plusieurs: opérations atomique et atomicité
   - fibo: notion de granularité: le découpage n'est pas gratuit:
     introduction au speedup (accélération)
   - fibo avec message: accélération, notion de placement et d'ordonnancement
     - placement statique: on recalcule sur chaque nœud et on schedule
       par round-robin (modulo): ils envoient le résultat aggloméré à 0.
     - placement dynamique: idem, avec un maître esclave, je démarre
       un esclave localement.

* Plan que je voudrais avoir
** une introduction générale à l'usage et aux différents cas (Calcul parallèle, Big Data et Cloud)
** les 4 piliers de Gilles Dowek 
*** Une partie architecture: 
    - intructions (out of order), vecteur, cœur, processeur, noeud,
      grappe, grille
    - accélérateurs: GPGPU
*** une partie algorithmique
    Regarder dans le cours d'Arnaud: notion de dépendance, chemein
    critique et travail Parler de compléxité séquentiel, l'étendre au
    travail. Notion d'efficacité, d'accélération et la loi de Amdhal.
    Parallélisme de tâche (control), parallélisme de données (data)
    Est-ce que j'ai le temps de faire un algorithme ?
    - Exemple de l'addition (illustre l'augmentation du travail et
      l'importance de la réduction de chemin critique
    - Ajout d'algorithme de répartition du travail: algorithme d'ordonnancement:
      - 3 types presentés:
	- statiques: fixe le lieu et la date des calcul
	- dynamique centralisé: maître esclave, à la BOINC. Idéal pour
          les calculs indépendants avec peu de données (Simulation de
          Monte-Carlo). L'esclave demande du travail et renvoie la
          réponse quand il a finit. Il redemande du travail.
	- dynamique distribué: vol de travail. Preuve en fonction
          potentiel que cela converge avec peu d'échange.
*** une partie donnée
    Stockage hierarchique, Mouvement de données, ne pas produire la
    donnée (In Situ)
    - Modèle numérique: découpage des données parfois complexe
      (prendre une image de scotch ou metis). Les frontières doivent
      être communiqués donc c'est mieux si la frontière est petite.
    - 
*** une partie langage
**** partir du modèle séquentiel: automate à état (mémoire)
     if, for, fonction, scalaire, tableau: 1 séquence
     d'instruction. la communication entre des deux partie du
     programme passe par l'écriture de la mémoire.
**** Le modèle parallèle: plusieurs séquence simultanées
***** Si elles ne lisent et modifient pas la même chose: aucune problème. problème paramétrique embarassingly parallel
***** Sinon on peut avoir des soucis
      par exemple en faisant en parallèle des additions
******  déclinaison en deux versions:
      1. synchronisation: on "séquentialise" les parties de code qui
         modifient à plusieurs des portions commune de l'état globale:
         blocage temporaire de certaines séquences pour n'avoir qu'une
         séquence à la fois modifiant les données communes
	 - Inconvénient: Il faut une mémoire commune.
      2. échange de message: les parties modifiées par chaque séquence
         sont strictement séparées, mais on échange des messages (Send
         et Recv) pour communiquer entre les séquences
	 Avantage: les messages peuvent circuler sur un réseau
    
