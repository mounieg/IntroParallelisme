LU_FLAVORS= LUALATEX
LU_MASTER= introParallelisme
PDFLATEX_OPTIONS= -shell-escape
LUALATEX_OPTIONS= -shell-escape

all: introParallelisme.pdf

include LaTeX.mk

introParallelisme.tex: introParallelisme.org
	emacs --visit=$< --batch --eval "(progn (setq org-latex-listings 'minted) (org-beamer-export-to-latex))"
