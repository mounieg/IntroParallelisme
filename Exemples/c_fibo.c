#include <stdlib.h>
#include <stdio.h>

#define PROFONDEUR 5


long long int fibo(int n) {
    if (n <= 2)
	return n;

    return fibo(n-1) + fibo(n-2);
}

int main(int argc, char **argv) {
    
    int n = 42;
    if (argc == 2)
	n = atoi(argv[1]);

    printf("%lld\n", fibo(n));
}
