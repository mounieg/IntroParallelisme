import time
import random

def dotp_fonctionnel(a,b):
    return sum(aterm * bterm for aterm,bterm in zip(a, b))

def dotp_iteratif(a,b):
    somme = 0.0
    for i in range(len(a)):
        somme += a[i] * b[i]
    return somme

def dotp_iteratif_copie(a,b):
    return sum( [ a[i] * b[i] for i in range(len(a)) ] )


a = [ random.random() for i in range( 300000000 )]
b = [ random.random() for i in range( 300000000 )]

debut = time.time()
val = dotp_fonctionnel(a, b)
fin = time.time()

print("valeur= ", val)
print("temps fonctionnel= ", fin - debut, " s")

debut = time.time()
val = dotp_iteratif(a, b)
fin = time.time()

print("valeur= ", val)
print("temps itératif= ", fin - debut, " s")

debut = time.time()
val = dotp_iteratif_copie(a, b)
fin = time.time()

print("valeur= ", val)
print("temps itératif= ", fin - debut, " s")
