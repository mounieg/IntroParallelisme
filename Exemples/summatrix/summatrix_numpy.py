import numpy as np
import time

a = np.zeros(shape=(10000,10000), dtype=np.float64)

a = a + np.random.rand(10000, 10000)

debut = time.time()
val = np.sum(a)
fin = time.time()

print("valeur= ", val)
print("temps= ", fin - debut, " s")

# to test
# a = np.add(a, np.random.random(size=300000000), dtype=float)
# b = np.add(b, np.random.random(size=300000000), dtype=float)
