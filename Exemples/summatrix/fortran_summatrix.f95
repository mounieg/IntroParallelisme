
pure function summatrix(x, n) result(v)
  implicit none
  integer, intent(in) :: n
  real*8, intent(in), dimension(n,n) :: x
  real*8 :: v
  integer :: i, j

  v = 0.0
  do j = 1, n
     do i = 1, n
        v = v + x(i,j)
     end do
  end do
end function summatrix

pure function summatrix_row(x, n) result(v)
  implicit none
  integer, intent(in) :: n
  real*8, intent(in), dimension(n,n) :: x
  real*8 :: v
  integer :: i, j

  v = 0.0
  do i = 1, n
     do j = 1, n
        v = v + x(i,j)
     end do
  end do
end function summatrix_row

pure function summatrix_sum(x, n) result(v)
  implicit none
  integer, intent(in) :: n
  real*8, intent(in), dimension(n,n) :: x
  real*8 :: v
  integer :: i, j

  v = sum(x)
end function summatrix_sum


function summatrix_omp(x, n) result(v)
  implicit none
  integer, intent(in) :: n
  real*8, intent(in), dimension(n,n) :: x
  real*8 :: v
  integer :: i, j

  v = 0.0
!$omp parallel do simd reduction(+:v) schedule(guided, 100)
  do j = 1, n
     do i = 1, n
        v = v + x(i,j)
     end do
  end do
!$omp end parallel do simd
end function summatrix_omp



subroutine vinit(x,n)
  real*8, intent(out), dimension(n,n) :: x
  integer, intent(in) :: n
  integer :: e

  x = reshape([(rand(0), e=1, n*n)], shape(x))
end subroutine vinit

program fortran_dotp
  implicit none
  real*8 :: summatrix
  real*8 :: summatrix_row
  real*8 :: summatrix_sum
  real*8 :: summatrix_omp
  integer, parameter :: TAILLE=10000
  real*8, allocatable :: x1(:,:)
  real*8 :: res, mydotp, mydotp_openmp
  integer*8 :: debut, fin, rate

  
  allocate(x1(TAILLE, TAILLE))
  call vinit(x1, TAILLE)
  ! mydopt(x1, x2, TAILLE) => res

  call system_clock(COUNT=debut, COUNT_RATE=rate)
  res = summatrix(x1, TAILLE)
  call system_clock(COUNT=fin)

    print '(A,E10.3, A, E10.3, A)', "fortran(summatrix) ", res, " en ", float(fin - debut)/(1.0*float(RATE)), " s"

  call system_clock(COUNT=debut, COUNT_RATE=rate)
  res = summatrix_row(x1, TAILLE)
  call system_clock(COUNT=fin)

  print '(A,E10.3, A, E10.3, A)', "fortran(summatrix per row) ", res, " en ", float(fin - debut)/(1.0*float(RATE)), " s"

  call system_clock(COUNT=debut, COUNT_RATE=rate)
  res = summatrix_sum(x1, TAILLE)
  call system_clock(COUNT=fin)

  print '(A,E10.3, A, E10.3, A)', "fortran(summatrix intrinsic sum) ", res, " en ", float(fin - debut)/(1.0*float(RATE)), " s"

  call system_clock(COUNT=debut, COUNT_RATE=rate)
  res = summatrix_omp(x1, TAILLE)
  call system_clock(COUNT=fin)

  print '(A,E10.3, A, E10.3, A)', "fortran(summatrix openmp) ", res, " en ", float(fin - debut)/(1.0*float(RATE)), " s"

  ! call system_clock(COUNT=debut, COUNT_RATE=rate)
  ! res = mydotp(x1, x2, TAILLE)
  ! call system_clock(COUNT=fin)

  ! print '(A,E10.3, A, E10.3, A)', "fortran(mydotp) ", res, " en ", float(fin - debut)/(1.0*float(RATE)), " s"

  ! call system_clock(COUNT=debut, COUNT_RATE=rate)
  ! res = mydotp_openmp(x1, x2, TAILLE)
  ! call system_clock(COUNT=fin)

  ! print '(A,E10.3, A, E10.3, A)', "fortran(mydotp_openmp) ", res, " en ", float(fin - debut)/(1.0*float(RATE)), " s"
  
  
end program fortran_dotp
