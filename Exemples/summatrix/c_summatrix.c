#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef MKL
#include <mkl/mkl.h>
#else
#include <cblas.h>
#endif

double summatrix(const int n, const double x[static n][n]) {
  double v = 0.0;
  for (int i = 0; i < n; i++) {
      for(int j = 0; j < n; j++)
	  v += x[i][j];
  }
  return v;
}

double summatrix_bad(const int n, const double x[static n][n]) {
  double v = 0.0;
  for (int j = 0; j < n; j++)
      for (int i = 0; i < n; i++)
	  v += x[i][j];
  return v;
}

/* typedef float v8sd __attribute((vector_size(64), __aligned__(64))); */

/* float dotp_vec(const int n, const v8sd x[static n][static n / 8]) { */
/*   double v = 0.0; */
/*   v8sf v8; */
/*   for (int i = 0; i < n; i++) */
/*       for (int j = 0; j < n / 8; j++) { */
/*     v8 = x[i] * y[i]; // ou __builtin_ia32_mulps256(x[i], y[i]); */
/*     float const *const v8f = (float *)&v8; */
/*     // for(int j=0; j < 8; j++) */
/*     //   v += v8f[j]; */
/*     v += v8f[0] + v8f[1] + v8f[2] + v8f[3] + v8f[4] + v8f[5] + v8f[6] + v8f[7]; */
/*   } */
/*   return v; */
/* } */

// manque: initialisation first touch
// schedule(dynamic) bug avec gcc-7
float summatrix_omp(const int n, const double x[static n][n]) {
  double v = 0.0;
  // schedule(guided,1000000)
#pragma omp parallel for reduction(+ : v) schedule(guided, 100)
  for (int i = 0; i < n; i++)
      for(int j=0; j < n; j++)
	  v += x[i][j];
  
  return v;
}

/* float dotp_openacc(const int n, const float x[static n], */
/*                    const float y[static n]) { */
/*   double v = 0.0; */
/* #pragma acc parallel loop reduction(+ : v) */
/*   for (int i = 0; i < n; i++) { */
/*     v += x[i] * y[i]; */
/*   } */
/*   return v; */
/* } */

/* float dotp_cblas(int n, float x[static n], float y[static n]) { */
/*   return cblas_sdot(n, x, 1, y, 1); */
/* } */

int main(int argc, char **argv) {
  int n = 10000;
  if (argc == 2)
    n = atoi(argv[1]);

  double (*x)[n] = aligned_alloc(512, sizeof(double[n][n])); // calloc(n, sizeof(float));

  assert(x);
  for (int i = 0; i < n; i++) {
      for(int j = 0; j < n; j++) {
	  x[i][j] = drand48();
      }
  }

  struct timespec debut, fin;
  clock_gettime(CLOCK_REALTIME, &debut);
  double v1 = summatrix(n, x);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("summatrix good line loop %lf en %g s\n", v1,
         (fin.tv_sec - debut.tv_sec) +
             (fin.tv_nsec - debut.tv_nsec) / 1000000000.0);

  clock_gettime(CLOCK_REALTIME, &debut);
  double v3 = summatrix_bad(n, x);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("summatrix bad row loop %lf en %g s\n", v3,
         (fin.tv_sec - debut.tv_sec) +
             (fin.tv_nsec - debut.tv_nsec) / 1000000000.0);

  clock_gettime(CLOCK_REALTIME, &debut);
  float v2 = summatrix_omp(n, x);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("omp %lf en %g s\n", v2,
         (fin.tv_sec - debut.tv_sec) +
             (fin.tv_nsec - debut.tv_nsec) / 1000000000.0);

  /* clock_gettime(CLOCK_REALTIME, &debut); */
  /* float v4 = dotp_cblas(n, x, y); */
  /* clock_gettime(CLOCK_REALTIME, &fin); */
  /* printf("cblas_sdot_openblas/mkl %f en %g s\n", v4, */
  /*        (fin.tv_sec - debut.tv_sec) + */
  /*            (fin.tv_nsec - debut.tv_nsec) / 1000000000.0); */

  /* clock_gettime(CLOCK_REALTIME, &debut); */
  /* float v5 = dotp_openacc(n, x, y); */
  /* clock_gettime(CLOCK_REALTIME, &fin); */
  /* printf("openacc %f en %g s\n", v5, */
  /*        (fin.tv_sec - debut.tv_sec) + */
  /*            (fin.tv_nsec - debut.tv_nsec) / 1000000000.0); */
}
