#include <stdio.h>
#include <omp.h>

int compteur=0;

#define NB 100000

void add() {
    compteur++;
}

void boucle( void(*f)() ) {
  for(int i=0; i < NB; i++)
    f();
}

int main(int argc, char **argv) {
  int nbth=1;
#pragma omp parallel shared(nbth)
  {
#pragma omp single
    nbth = omp_get_num_threads();

    boucle(add);
  }
  printf("%d / %d (%d threads)\n", compteur, NB * nbth, nbth);

}
