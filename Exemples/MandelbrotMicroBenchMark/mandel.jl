
# idée venant de: High Performance Julia, Avik Sengupta, Packt 2019

function mandel(c)
    z  = c
    maxiter = 80
    for n in 1:maxiter
        if abs(z) > 2
            return n-1
        end
        z = z^2 +c
    end
    return maxiter
end

# interval à calculer: -2;1 + i -1:1

