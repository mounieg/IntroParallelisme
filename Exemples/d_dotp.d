import std.stdio : writeln;
import std.conv;
import std.random: uniform01;
import std.datetime;
import std.numeric;
import std.algorithm;
import std.datetime;
import std.datetime.stopwatch : benchmark, StopWatch;

float dotp(float[] x, float[] y)
{
	double v=0.0;
	assert(x.length == y.length);
	foreach(i; 0 .. x.length) {
		v += x[i] * y[i];
	}
	return v;
}

float dotp_dup(float[] x, float[] y)
{
	assert(x.length == y.length);
	auto z = new float[x.length];
        z[] = x[] * y[];
	return z.sum();
}


float dotp_numeric(float[] x, float[] y)
{
	return x.dotProduct(y);
}

void main(string[] args) {
	int n= 300_000_000;
	if (args.length == 2)
		n = to!int(args[1]);
	
	auto x = new float[n];
	auto y = new float[n];

	foreach(i; 0 .. x.length) {
	    x[i] = uniform01();
	    y[i] = uniform01();
	}
	
	float v1;
	void f0() {
		v1 = dotp(x, y);
	}
	float v2;
	void f2() {
		v2 = dotp_numeric(x, y);
	}
	float v3;
	void f1() {
		v3 = dotp_dup(x, y);
	}
	auto temps = benchmark!(f0,f2, f1)(1);
	writeln("for loop\t", v1, " en ", temps[0].total!"nsecs", " ns");
	writeln("numeric \t", v2, " en ", temps[1].total!"nsecs", " ns");
	writeln("dup array op\t", v3, " en ", temps[2].total!"nsecs", " ns");

}
