#include <stdio.h>
#include <omp.h>

int compteur = 0;
int compteurlock = 0;
int compteuratomic = 0;

#define NB 100000

void add() {
  compteur++;
}


omp_lock_t mutex;
void add_lock() {
  omp_set_lock(& mutex);
  compteurlock++;
  omp_unset_lock(& mutex);
}

void add_atomic() {
#pragma omp atomic
  compteuratomic++;
}



void boucle( void(*f)() ) {
  for(int i=0; i < NB; i++)
    f();
}

int main(int argc, char **argv) {
  int nbth=1;
  double debut, fin;

  omp_init_lock(& mutex);
#pragma omp parallel shared(nbth) private(debut, fin)
  {
#pragma omp single
    nbth = omp_get_num_threads();

#pragma omp barrier

    // FAUSSE
#pragma omp master
    debut = omp_get_wtime();

    boucle(add);

#pragma omp barrier
#pragma omp master
    {
      fin = omp_get_wtime();
      printf("none: %d / %d (%d threads) en %g s\n", compteur,
	     NB * nbth, nbth, fin - debut);
    }
    
#pragma omp barrier

    // VERROU
#pragma omp master
    debut = omp_get_wtime();

    boucle(add_lock);

#pragma omp barrier
#pragma omp master
    {
      fin = omp_get_wtime();
      printf("lock: %d / %d (%d threads) en %g s\n", compteurlock,
	     NB * nbth, nbth, fin - debut);
    }

#pragma omp barrier

    // ATOMIC
    // VERROU
#pragma omp master
    debut = omp_get_wtime();

    boucle(add_atomic);

#pragma omp barrier
#pragma omp master
    {
      fin = omp_get_wtime();
      printf("atomic: %d / %d (%d threads) en %g s\n", compteuratomic,
	     NB * nbth, nbth, fin - debut);
    }
#pragma omp barrier    
  }
}
