package main

import fmt "fmt"

func fibo_seq(n int) int {
	if n < 2 {
		return n;
	}
	return fibo_seq(n - 1) + fibo_seq(n - 2)
}



func fibo(n int, prof int, r chan int) {
	if n < 2 {
		r <- n;
		return;
	}
	if prof == 0 {
		r <- fibo_seq(n)
		return;
	}
	var res1 chan int = make(chan int)
	var res2 chan int = make(chan int, 1)
	go fibo(n - 2, prof-1, res1)
	fibo(n - 1, prof -1, res2)
	
	r <- ( <- res1 + <- res2 )
}

func main() {
	var r chan int = make(chan int)
	go fibo(42, 5, r)
	fmt.Printf("%d\n", <- r)
}
