
pure function mydotp(x, y, n) result(v)
  implicit none
  integer, intent(in) :: n
  real, intent(in), dimension(n) :: x, y
  real*8 :: v
  integer :: i

  v = 0.0
  do i = 1, n
     v = v + x(i) * y(i)
  end do
end function mydotp


function mydotp_openmp(x, y, n) result(v)
  implicit none
  integer, intent(in) :: n
  real, intent(in), dimension(n) :: x, y
  real*8 :: v
  integer :: i

  v = 0.0
!$omp parallel do simd reduction(+:v)
  do i = 1, n
     v = v + x(i) * y(i)
  end do
!$omp end parallel do simd
end function mydotp_openmp



subroutine vinit(x,n)
  real, intent(out), dimension(n) :: x
  integer, intent(in) :: n
  integer :: e

  x = [(rand(0), e=1, n)]
end subroutine vinit

program fortran_dotp
  implicit none
  integer, parameter :: TAILLE=300000000
  real, allocatable :: x1(:), x2(:)
  real*8 :: res, mydotp, mydotp_openmp
  integer*8 :: debut, fin, rate

  
  allocate(x1(TAILLE))
  allocate(x2(TAILLE))
  call vinit(x1, TAILLE)
  call vinit(x2, TAILLE)
  ! mydopt(x1, x2, TAILLE) => res

  call system_clock(COUNT=debut, COUNT_RATE=rate)
  res = dot_product(x1, x2)
  call system_clock(COUNT=fin)

    print '(A,E10.3, A, E10.3, A)', "fortran(dot_product) ", res, " en ", float(fin - debut)/(1.0*float(RATE)), " s"

  call system_clock(COUNT=debut, COUNT_RATE=rate)
  res = mydotp(x1, x2, TAILLE)
  call system_clock(COUNT=fin)

  print '(A,E10.3, A, E10.3, A)', "fortran(mydotp) ", res, " en ", float(fin - debut)/(1.0*float(RATE)), " s"

  call system_clock(COUNT=debut, COUNT_RATE=rate)
  res = mydotp_openmp(x1, x2, TAILLE)
  call system_clock(COUNT=fin)

  print '(A,E10.3, A, E10.3, A)', "fortran(mydotp_openmp) ", res, " en ", float(fin - debut)/(1.0*float(RATE)), " s"
  
  
end program fortran_dotp
