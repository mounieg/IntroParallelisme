package main

// installation de gonum/mat dans le module
// mkdir godot
// cd godot
// go init godot
// go get gonum.org/v1/gonum@latest

import (
	"fmt"
	"math"
	"time"
	"gonum.org/v1/gonum/mat"
)

func dotp(x,y []float32) float64 {
	var som float64
	for i,_ := range x {
		som += float64(x[i] * y[i])
	}
	return som
}

func dotp2(x,y []float32) float64 {
	var som float64
	for i,v := range x {
		som += float64(v * y[i])
	}
	return som
}

func dotp_mat(x,y mat.Vector) float64 {
	var som float64
	som = mat.Dot(x, y)
	return som
}


func main() {
	var a []float32
	var b []float32

	a = make([]float32, 300000000)
	b = make([]float32, 300000000)

	for i,_ := range a {
		a[i] = float32((int(math.Floor(float64(123 * i + 5))) % 97)) / 97.0
		b[i] = float32((int(math.Floor(float64(41 * i + 5))) % 97)) / 97.0
	}
	start := time.Now()
	var res = dotp2(a, b)
	end := time.Now()
	fmt.Println("valeur range x * y[i]", res, " en ", end.Sub(start))

	start = time.Now()
	res = dotp(a, b)
	end = time.Now()
	fmt.Println("valeur x[i] * y[i]", res, " en ", end.Sub(start))

	
	// un seul vecteur décaler de 1 pour gagner le facteur 2 en mémorie
	var a64 []float64
	a64 = make([]float64, 300000001)
	for i,_ := range a64 {
		a64[i] = float64((int(math.Floor(float64(123 * i + 5))) % 97)) / 97.0
	}

	ma := mat.NewVecDense(300000000, a64[0:300000000])
	mb := mat.NewVecDense(300000000, a64[1:])
	start = time.Now()
	res = dotp_mat(ma, mb)
	end = time.Now()
	fmt.Println("mat library (32bits n'existe pas, 64bits, x2 en taille)", res, " en ", end.Sub(start))


}
