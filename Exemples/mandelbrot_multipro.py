import sys
import numpy as np
import matplotlib.pyplot as plt
from multiprocessing import Pool

def mandelbrot(c):
    """Returns an image of the Mandelbrot fractal of size (h,w)."""
    z = c
    divtime = maxit + np.zeros(z.shape, dtype=int)

    for i in range(maxit):
        z = z**2 + c
        diverge = z*np.conj(z) > 2**2           # who is diverging
        div_now = diverge & (divtime == maxit)  # who is diverging now
        divtime[div_now] = i                    # note when
        z[diverge] = 2                          # avoid diverging too much

    return divtime


h=2000
w=2000
maxit=100
poolsize= 1
if len(sys.argv) > 1:
    poolsize = int(sys.argv[1])
    
y, x = np.ogrid[-1.4:1.4:h*1j, -2:0.8:w*1j]
c = x+y*1j
with Pool(poolsize) as p:
    r = p.map(mandelbrot, c)

plt.imshow(r)
plt.show()
