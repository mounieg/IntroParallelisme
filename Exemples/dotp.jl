using LinearAlgebra

function dotp(x,y)
    v = 0.0
    for i=1:length(x)
        @inbounds v += x[i] * y[i]
    end
    return v
end


function dotp_simd(x,y)
    v = 0.0
    @simd for i= 1:length(x)
        @inbounds v += x[i] * y[i]
    end
    return v
end

function dotp_dotop(x,y)
    v = reduce(+, x .* y) # lent car allocation d'un temporaire !
    return v
end

function dotp_intrinsic(x,y)
    v = LinearAlgebra.dot(x, y)
    return v
end

n = 300000000
x = rand(Float32,n)
y = rand(Float32,n)

dotp(x,y)
dotp_simd(x,y)
dotp_dotop(x, y)

println("for loop")
@time v1 = dotp(x,y)
println(v1)

println("simd loop")
@time v2 = dotp_simd(x,y)
println(v2)

println("functional")
@time v3 = dotp_dotop(x,y)
println(v3)

println("linear algebra")
@time v4 = dotp_intrinsic(x,y)
println(v4)
