
function fibo(n)
    if (n < 2)
        return n
    end
    return fibo(n-1) + fibo(n-2)
end


function fibo_par_cut(n, c, v)
    if (n < 2)
        return v[1] += n
    end
    if (v < 1)
        put!(c, fibo(n) )
        return
    end

    c1 = Channel{Int}(1)
    c2 = Channel{Int}(1)
    @schedule fibo_par_cut(n-1, c1, v-1)
    @schedule fibo_par_cut(n-2, c2, v-1)

    put!(c, take!(c1) + take!(c2))
end

# println(Threads.nthreads())

n = 42

@time v = fibo(n)
println(v)

res = Channel{Int}(1)
@time fibo_par_cut(n, res, 5)
println(take!(res))
