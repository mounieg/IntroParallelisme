extern crate rand;
use rand::prelude::*;
use rand::rngs::SmallRng;
use std::env;
extern crate time;
extern crate rayon;
// use std::time::{Duration, Instant};
use std::time::{Instant};
use rayon::prelude::*;


fn dotwp(a : &[f32], b : &[f32]) -> f32 {
    a.par_iter().zip(b.par_iter()).map(|(a,b)| (*a * *b)as f32).sum()
}

fn dotw(a : &[f32], b : &[f32]) -> f32 {
    a.iter().zip(b.iter()).map(|(a,b)| (*a * *b)as f32).sum()
}

fn dotpf(a : &[f32], b : &[f32]) -> f32 {
    let mut sum = 0.0f32;
    for iter in a.iter().zip(b.iter()) {
        sum += (*iter.0 * *iter.1) as f32;
    }
    sum
}

fn dotp(a: &[f32], b: &[f32], n: usize) -> f32 {
    let mut sum = 0.0f32;
    for i in 0..n {
        sum += (a[i] * b[i]) as f32;
    }
    sum
}

fn main() {
    let mut n: usize = 300_000_000;
    if let Some(arg1) = env::args().nth(1) {
        n = arg1.parse::<usize>().unwrap();
    }
    let mut v1 = vec![0.0f32; n];
    let mut v2 = vec![0.0f32; n];

    println!("init ...");

    let mut rng = SmallRng::from_entropy();
    for x in v1.iter_mut().zip(v2.iter_mut()) {
        *x.0 = rng.gen();
        *x.1 = rng.gen();
    }

    println!("... done");

    let dt = Instant::now();
    let mut sum = dotpf(&v1, &v2);
    let ft = Instant::now();
    println!("fonctionnel: {} {:?}", sum, ft.duration_since(dt));

    let dt2 = Instant::now();
    sum = dotp(&v1, &v2, n);
    let ft2 = Instant::now();

    println!("iter: {} {:?}", sum, ft2.duration_since(dt2));

    let dt3 = Instant::now();
    sum = dotw(&v1, &v2);
    let ft3 = Instant::now();

    println!("dot product (Fred's way): {} {:?}", sum, ft3.duration_since(dt3));

    let dt4 = Instant::now();
    sum = dotwp(&v1, &v2);
    let ft4 = Instant::now();

    println!("dot product (Fred's rayon way): {} {:?}", sum, ft4.duration_since(dt4));
}

