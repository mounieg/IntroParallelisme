class dotp
{
    public static double ps(float a[], float b[], int n) {
	double v=0.0;
	for(int i=0; i < n; i++) {
	    v += a[i] * b[i];
	}
	return v;
    }
    
    public static void main(String args[])
	{
	    int n= 300000000;
	    float [] a = new float[n];
	    float [] b = new float[n];

	    java.util.Random gen = new java.util.Random();
	    for(int i=0; i < n; i++) {
		a[i] = gen.nextFloat();
		b[i] = gen.nextFloat();
	    }
	    
	    long debut = System.nanoTime();
	    double res= dotp.ps(a, b, n);
	    long fin = System.nanoTime();
	    System.out.printf("%g en %d ns\n", res, fin - debut);
	}
}
