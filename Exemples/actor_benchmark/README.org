#+options: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+options: author:t broken-links:nil c:nil creator:nil
#+options: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+options: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+options: timestamp:t title:t toc:t todo:t |:t
#+title: Actor benchmark
#+date: <2021-07-26 lun.>
#+author: Grégory Mounié
#+email: Gregory.Mounie@imag.fr
#+language: fr
#+select_tags: export
#+exclude_tags: noexport
#+creator: Emacs 27.1 (Org mode 9.4)

* Actor Benchmark
  L'idée vient du code Erlang, Java, Go Haskell, Pikka disponible là
  https://github.com/jeanparpaillon/actors_benchmark

** Autres implantations possibles
   Partir de la version en Go ?
**** OpenMP en C
**** C++ avec thread::async
     - et peut-être un peu de fonctionnel ?
       
