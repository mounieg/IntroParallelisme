#include <stdio.h>
#include <math.h>
#include <omp.h>

double pi(int n);
void term(double *, double);

int main() {
#pragma omp parallel
#pragma omp single
	printf("%lg\n", pi(500000));
}

// pi launches n openmp tasks to compute an
// approximation of pi.

double pi(int n) {
	double f=0;
#pragma omp taskloop shared(f)
	for(int k=0; k < n; k++) {
		term(&f, k);
	}

#pragma omp taskwait
	return f;
}


void term(double *const val, double const k) {
	double tmp= 4.0 * pow(-1, k) / (2*k + 1);

#pragma omp atomic
	*val += tmp;
}

