package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(pi(500000))
}

// pi launches n goroutines to compute an
// approximation of pi.
func pi(n int) float64 {
	f := 0.0
	for k := 0; k <= n; k++ {
		f+= term( float64(k) )
	}
	return f
}

func term(k float64) float64 {
	return 4 * math.Pow(-1, k) / (2*k + 1)
}
