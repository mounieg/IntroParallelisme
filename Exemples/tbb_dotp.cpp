#include <tbb/tbb.h>
#include <sstream>
#include <cstdlib>

using namespace tbb;

class MyDotp {
    float *x, *y;
    int n;
public:
    double my_dotp;

    void operator() ( const blocked_range<int>& r ) {
	double locdotp = my_dotp; // same object multiple time is possible
	auto end = r.end();
	for(auto i=r.begin(); i != end; i++) {
	    locdotp +=  x[i] * y[i];
	}
        my_dotp = locdotp;
    }
    
    void join( const MyDotp& y) { my_dotp += y.my_dotp; }

    MyDotp( MyDotp &d, tbb::split): x(d.x), y(d.y), n(d.n), my_dotp(0) {};
    
    MyDotp(float *x, float *y, int n): x(x), y(y), n(n), my_dotp(0) {};  
};

double dotp(float *x, float *y, int n) {
    MyDotp v(x, y, n);
    parallel_reduce( blocked_range<int>(0, n),
		     v);
    return v.my_dotp;
}

int main(int argc, char**argv) {
  int n= 300000000;
  if (argc == 2) {
      std::istringstream istr(argv[1]);
      istr >> n;
  }

  float *x = new float[n];
  float *y = new float[n];

  for(int i=0; i < n; i++) {
    x[i] = drand48();
    y[i] = drand48();
  }

  struct timespec debut, fin;
  clock_gettime(CLOCK_REALTIME, &debut);
  float v1 = dotp(x, y, n);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("tbb reduce loop %f en %g s\n", v1, (fin.tv_sec - debut.tv_sec)
         +(fin.tv_nsec - debut.tv_nsec)/ 1000000000.0);

}
