#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <numeric>
#include <valarray>
#include <chrono>

float dotp(float  *x, float  *y, int n) {
  double v=0.0;
  for(int i=0; i < n; i++) {
    v += x[i] * y[i];
  }
  return v;
}

float dotp_numeric(float  *x, float  *y, int  n) {
	return  std::inner_product(x, x+n, y, 0.0);
}


float dotp_valarray(std::valarray<float> x, std::valarray<float> y) {
	return (x * y).sum();
}


int main(int argc, char**argv) {
  int n= 300000000;
  if (argc == 2)
    n = atoi(argv[1]);

  float *x = new float[n];
  float *y = new float[n];

  for(int i=0; i < n; i++) {
    	  x[i] = std::rand()/(float)RAND_MAX;
	  y[i] = std::rand()/(float)RAND_MAX;

  }

  auto debut = std::chrono::high_resolution_clock::now();
  float v1 = dotp(x, y, n);
  auto fin = std::chrono::high_resolution_clock::now();
  
  std::cout << "for loop " << v1 << " en " <<
	  std::chrono::duration<double>(fin-debut).count() <<
	  std::endl;

  debut = std::chrono::high_resolution_clock::now();
  float v2 = dotp_numeric(x, y, n);
  fin = std::chrono::high_resolution_clock::now();
  
  std::cout << "std::numeric::inner_product " << v2 << " en " <<
	  std::chrono::duration<double>(fin-debut).count() <<
	  std::endl;

  delete [] x;
  delete [] y;

  std::valarray<float> vx(n);
  std::valarray<float> vy(n);

  for(int i=0; i < n; i++) {
	  vx[i] = std::rand()/(float)RAND_MAX;
	  vy[i] = std::rand()/(float)RAND_MAX;
  }

  debut = std::chrono::high_resolution_clock::now();
  float v3 = dotp_valarray(vx, vy);
  fin = std::chrono::high_resolution_clock::now();
  
  std::cout << "std::valarray " << v3 << " en " <<
	  std::chrono::duration<double>(fin-debut).count() <<
	  std::endl;
}
