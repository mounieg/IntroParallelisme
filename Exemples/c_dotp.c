#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef MKL
#include <mkl/mkl.h>
#define BLASLIB "mkl"
#else
#include <cblas.h>
#define BLASLIB "openblas"
#endif

float dotp(const int n, const float x[static n], const float y[static n]) {
  double v = 0.0;
  for (int i = 0; i < n; i++) {
    v += x[i] * y[i];
  }
  return v;
}

typedef float v8sf __attribute((vector_size(32), __aligned__(32)));

float dotp_vec(const int n, const v8sf x[static n / 8],
               const v8sf y[static n / 8]) {
  double v = 0.0;
  v8sf v8;
  for (int i = 0; i < n / 8; i++) {
    v8 = x[i] * y[i]; // ou __builtin_ia32_mulps256(x[i], y[i]);
    float const *const v8f = (float *)&v8;
    // for(int j=0; j < 8; j++)
    //   v += v8f[j];
    v += v8f[0] + v8f[1] + v8f[2] + v8f[3] + v8f[4] + v8f[5] + v8f[6] + v8f[7];
  }
  return v;
}

// manque: initialisation first touch
// schedule(dynamic) bug avec gcc-7
float dotp_omp(const int n, const float x[static n], const float y[static n]) {
  double v = 0.0;
  // schedule(guided,1000000)
#pragma omp parallel for reduction(+ : v) schedule(guided, 1000000)
  for (int i = 0; i < n; i++) {
    v += x[i] * y[i];
  }
  return v;
}

float dotp_omp_openblas(const int n, const float x[static n], const float y[static n]) {
  double v=0.0;
#pragma omp parallel
#pragma omp single
  {
	v= cblas_sdot(n, x, 1, y, 1);
  }
  return v;
}

float dotp_openacc(const int n, const float x[static n],
                   const float y[static n]) {
  double v = 0.0;
#pragma acc parallel loop reduction(+ : v)
  for (int i = 0; i < n; i++) {
    v += x[i] * y[i];
  }
  return v;
}

float dotp_cblas(int n, float x[static n], float y[static n]) {
  return cblas_sdot(n, x, 1, y, 1);
}

int main(int argc, char **argv) {
  int n = 300000256;
  if (argc == 2)
    n = atoi(argv[1]);

  float *x = aligned_alloc(512, sizeof(float[n])); // calloc(n, sizeof(float));
  float *y = aligned_alloc(512, sizeof(float[n])); // calloc(n, sizeof(float));

  assert(x);
  assert(y);
  for (int i = 0; i < n; i++) {
    x[i] = drand48();
    y[i] = drand48();
  }

  struct timespec debut, fin;
  clock_gettime(CLOCK_REALTIME, &debut);
  float v1 = dotp(n, x, y);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("for loop %f en %g s\n", v1,
         (fin.tv_sec - debut.tv_sec) +
             (fin.tv_nsec - debut.tv_nsec) / 1000000000.0);

  clock_gettime(CLOCK_REALTIME, &debut);
  float v3 = dotp_omp(n, x, y);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("openmp %f en %g s\n", v3,
         (fin.tv_sec - debut.tv_sec) +
             (fin.tv_nsec - debut.tv_nsec) / 1000000000.0);

  clock_gettime(CLOCK_REALTIME, &debut);
  float v3o = dotp_omp_openblas(n, x, y);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("openmp+%s %f en %g s\n", BLASLIB, v3o, (fin.tv_sec - debut.tv_sec)
	 +(fin.tv_nsec - debut.tv_nsec)/ 1000000000.0);

  
  clock_gettime(CLOCK_REALTIME, &debut);
  float v2 = dotp_vec(n, (v8sf *)x, (v8sf *)y);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("vector %f en %g s\n", v2,
         (fin.tv_sec - debut.tv_sec) +
             (fin.tv_nsec - debut.tv_nsec) / 1000000000.0);

  clock_gettime(CLOCK_REALTIME, &debut);
  float v4 = dotp_cblas(n, x, y);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("cblas_sdot_%s %f en %g s\n", BLASLIB, v4, (fin.tv_sec - debut.tv_sec)
	 +(fin.tv_nsec - debut.tv_nsec)/ 1000000000.0);

  clock_gettime(CLOCK_REALTIME, &debut);
  float v5 = dotp_openacc(n, x, y);
  clock_gettime(CLOCK_REALTIME, &fin);
  printf("openacc %f en %g s\n", v5,
         (fin.tv_sec - debut.tv_sec) +
             (fin.tv_nsec - debut.tv_nsec) / 1000000000.0);
}
