import numpy as np
import time

a = np.zeros(shape=(300000000,), dtype=float)
b = np.zeros(shape=(300000000,), dtype=float)

a = a + np.random.random(size=300000000)
b = b + np.random.random(size=300000000)



debut = time.time()
val = np.dot(a, b)
fin = time.time()

print("valeur= ", val)
print("temps= ", fin - debut, " s")

# to test
# a = np.add(a, np.random.random(size=300000000), dtype=float)
# b = np.add(b, np.random.random(size=300000000), dtype=float)
