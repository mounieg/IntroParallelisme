#include <thread>
#include <iostream>
#include <functional>

using namespace std;

int compteur=0;

void add(int val) {
    compteur += val;
}

void boucle(function <void(int)> f,  int nb, int val) {
  for(int i=0; i < nb; i++)
    f(val);
}

int main(int argc, char **argv) {
  int nb= 20000;
  int val = 1;
  
  if (argc == 4) {
    nb = stoi(argv[1]);
    val = stoi(argv[2]);
  }

  auto t1 = new thread(boucle, add, nb, val);
  auto t2 = new thread(boucle, add, nb, val);
  
  t1->join(); t2->join();
  cout << compteur << endl;
}
    
