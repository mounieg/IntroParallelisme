#include <boost/mpi.hpp>
#include <cstdint>
#include <cassert>
#include <iostream>
using namespace std;

namespace mpi = boost::mpi;
uint32_t NB = 0;
uint32_t ME = -1;

uint64_t fibo(uint32_t n) {
    if ( n < 2 )
	return n;    
    return fibo( n - 1 ) + fibo( n - 2 );
}

uint32_t nbtasks = 0; // chaque processus compte toutes les tâches
const int TAG= 123;

uint64_t fibo_parallel(uint32_t n, uint32_t p, mpi::communicator &world) {
    if ( n < 2 )
	return n;

    uint64_t val = 0;
    // Ordonnancement statique
    if ( p == 0 ) {
	if ( nbtasks % NB == ME ) {
	    uint64_t val = fibo( n - 1 );
	    world.send(0, TAG, val);
	}
	nbtasks ++;
	
	if ( nbtasks % NB == ME ) {
	    uint64_t val = fibo( n - 2 );
	    world.send(0, TAG, val);
	}
	nbtasks ++;

	if ( ME == 0 ) {
	    uint64_t val1, val2;
	    world.recv( (nbtasks - 2)%NB, TAG, val1);
	    world.recv( (nbtasks - 1)%NB, TAG, val2);
	    return val1 + val2;
	}	    
    } else {
	val = fibo_parallel( n-1, p-1, world) + fibo_parallel( n-2, p-1, world);
	return val;
    }
    return 0;
}

int main() {
    mpi::environment env;
    mpi::communicator world;
    
    ME = world.rank();
    NB = world.size();

    uint64_t val = fibo_parallel( 42, 5, world );
    if ( ME == 0 ) {
	cout << val << endl;
    } else {
	assert( val == 0 );
    }
    return 0;
}
