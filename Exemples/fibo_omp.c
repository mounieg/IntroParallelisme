#include <stdlib.h>
#include <stdio.h>

#define PROFONDEUR 5

long long int fibo_sequentiel(int n) {
    if (n <= 2)
	return n;

    return fibo_sequentiel(n-1) + fibo_sequentiel(n-2);
}
    

long long int fibo_parallele(int n, int prof) {
    if (n <= 2)
	return 1;

    if (prof <= 0)
	return fibo_sequentiel(n-1) + fibo_sequentiel(n-2);
    
    long long int r1;
    
#pragma omp task shared(r1) firstprivate(n, prof)
    r1 = fibo_parallele(n-2, prof -1);

    long long int r2 = fibo_parallele(n-1, prof -1);

#pragma omp taskwait
    return r1 + r2;
}

int main(int argc, char **argv) {
    
    int n = 42;
    if (argc == 2)
	n = atoi(argv[1]);
#pragma omp parallel
#pragma omp single
    printf("%lld\n", fibo_parallele(n, PROFONDEUR));
}
