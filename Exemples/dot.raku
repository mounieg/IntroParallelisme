#!/usr/bin/raku

# prérequis: installer la bibliothèque libgsl/blas avec zef
# première exécution de zef assez longue: plusieurs minutes
# zef install Math::Libgsl::BLAS


# version avec GSL
# https://github.com/frithnanth/raku-Math-Libgsl-BLAS/tree/master/lib/Math/Libgsl/BLAS

use Math::Libgsl::Vector::Num32;
use Math::Libgsl::BLAS::Num32;

my $x = Math::Libgsl::Vector::Num32.new: 300_000_000;
my $y = Math::Libgsl::Vector::Num32.new: 300_000_000;
$x.setall(.1);
$y.setall(.2);

my $deb = now;
my $v = sdot($x, $y);
my $fin = now;
say "$v en " ~ $fin - $deb ~ " s";

